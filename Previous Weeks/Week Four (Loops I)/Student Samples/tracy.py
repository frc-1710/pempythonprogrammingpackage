# #Exercise: Take a user input of an integer.
# #Create a dictionary or list of words.
# #Output the words corresponding to the values up to and including the user input.

# sentence=['The','quick', 'brown', 'fox', 'jumped', 'over', 'the', 'lazy', 'dogs']
# bound = input("Enter the number of the last word you want to see:  ")
# for count in xrange(bound):
#     count = count + 1
#     print sentence[count-1]
    
#########################
#Exercise
#Ask user for a numerical grade
#Print letter grade

percent_grade = input("Give me your numerical grade. ")
if percent_grade >=90:
    letter_grade = "A"
elif percent_grade >=80:
    letter_grade = "B"
elif percent_grade >=70:
    letter_grade = "C"
elif percent_grade >=60:
    letter_grade = "D"
else:
    letter_grade = "F"

print (letter_grade)