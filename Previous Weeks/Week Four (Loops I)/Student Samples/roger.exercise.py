#!/usr/bin/python

dictionary = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
              "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

index = int(raw_input("Please input an integer between 1 and 26 inclusive: "))

if (index <= 0) or (index > 26):
    print "Input out of range!"
    exit()

for i in xrange(index):
    print dictionary[i],

someInput = raw_input("Type 3 to stop, anything else to continue: ")

while someInput != '3':
    print "Thank you for not typing 3. I HATE 3."
    print "You typed: " + someInput
    print "\nType 3 to stop, anything else to continue: "
    someInput = raw_input()

print "How dare you! That is a 3! I QUIT!"

