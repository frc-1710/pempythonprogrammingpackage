print "-------------- Exercise 1  -------------------"

#take in user input for name
user_input = str(raw_input("Enter your firstname and lastname with a space in between :"))

#split string to get array of string
user_input_array = user_input.split()

#check to ensure input is valid
if len(user_input_array) == 2:
    #get firstname and lastname
    firstname = user_input_array[0]
    lastname = user_input_array[1]
    
    #get length of firstname and lastname
    firstname_length = len(firstname)
    lastname_length = len(lastname)
    
    #get initials
    initials = firstname[0] + lastname[0]
    
    #print output
    print "First name is", firstname, "\nLast name is", lastname
    print "Length of firstname is", firstname_length, "\nLast of lastname is", lastname_length
    print "Initals is", initials
    
else:
    
    print "Invalid input"