# example of while loop

# take input from the user
someInput = raw_input("Type 3 to continue, anything else to quit: ")

while someInput == '3':
    print("Thank you for the 3. It was very kind of you!")
    print("\nType 3 to continue, anything else to quit: ")
    someInput = raw_input()

print("How dare you! That is not a 3! I QUIT!")


# While is not 3 continue in loop, if it is 3 break loop ... While loop true
# print the value of the loop.