import random

# for each card: used?, card face, value
deck = [ [ 0, "Ac", 1 ],
         [ 0, "2c", 2 ],
         [ 0, "3c", 3 ],
         [ 0, "4c", 4 ],
         [ 0, "5c", 5 ],
         [ 0, "6c", 6 ],
         [ 0, "7c", 7 ],
         [ 0, "8c", 8 ],
         [ 0, "9c", 9 ],
         [ 0, "10c", 10 ],
         [ 0, "Jc", 10 ],
         [ 0, "Qc", 10 ],
         [ 0, "Kc", 10 ],
         [ 0, "Ad", 1 ],
         [ 0, "2d", 2 ],
         [ 0, "3d", 3 ],
         [ 0, "4d", 4 ],
         [ 0, "5d", 5 ],
         [ 0, "6d", 6 ],
         [ 0, "7d", 7 ],
         [ 0, "8d", 8 ],
         [ 0, "9d", 9 ],
         [ 0, "10d", 10 ],
         [ 0, "Jd", 10 ],
         [ 0, "Qd", 10 ],
         [ 0, "Kd", 10 ],
         [ 0, "Ah", 1 ],
         [ 0, "2h", 2 ],
         [ 0, "3h", 3 ],
         [ 0, "4h", 4 ],
         [ 0, "5h", 5 ],
         [ 0, "6h", 6 ],
         [ 0, "7h", 7 ],
         [ 0, "8h", 8 ],
         [ 0, "9h", 9 ],
         [ 0, "10h", 10 ],
         [ 0, "Jh", 10 ],
         [ 0, "Qh", 10 ],
         [ 0, "Kh", 10 ],
         [ 0, "As", 1 ],
         [ 0, "2s", 2 ],
         [ 0, "3s", 3 ],
         [ 0, "4s", 4 ],
         [ 0, "5s", 5 ],
         [ 0, "6s", 6 ],
         [ 0, "7s", 7 ],
         [ 0, "8s", 8 ],
         [ 0, "9s", 9 ],
         [ 0, "10s", 10 ],
         [ 0, "Js", 10 ],
         [ 0, "Qs", 10 ],
         [ 0, "Ks", 10 ] ]
# ace_elements = [ 0, 13, 26, 39 ]
dealer_hand = []
player_hand = []
dealer_stands_value = 17
max_value = 21

def deal_card() :
    not_unique = True
    
    # keep going until we generator a card that hasn't been dealt
    while( not_unique ) :
        temp_card = random.randint( 0, len( deck ) - 1 )
        if deck[ temp_card ][ 0 ] == 0 :
            not_unique = False
            deck[ temp_card ][ 0 ] = 1
            return temp_card
        # else : # debug
        #     print( deck[ temp_card ][ 1 ] + " has already been dealt." ) # debug
# deal_card()

def calc_total( this_hand ) :
    this_total = 0
    num_aces = 0
    
    for this_card in this_hand :
        this_value = deck[ this_card ][ 2 ]
        if this_value == 1 :
            # add aces later as value may be 1 or 11
            num_aces += 1
        else :
            this_total += this_value

    if num_aces > 1 :
        if ( ( num_aces - 1 ) + this_total ) > 10 :
            # would bust if ace valued at 11
            this_total += num_aces
        else :
            this_total = this_total + ( num_aces - 1 ) + 11
    elif num_aces == 1 :
        if this_total > 10 :
            # would bust if ace valued at 11
            this_total += 1
        else :
            this_total += 11
            
    return this_total
# calc_total()

def dealer_has_soft_17( this_hand ) :
    this_total = 0
    num_aces = 0
    
    for this_card in this_hand :
        this_value = deck[ this_card ][ 2 ]
        if this_value == 1 :
            # add aces later as value may be 1 or 11
            num_aces += 1
        else :
            this_total += this_value

    if num_aces > 1 :
        if ( ( num_aces - 1 ) + this_total ) > 10 :
            # would bust if ace valued at 11
            return False
        else :
            return True
    elif num_aces == 1 :
        if this_total > 10 :
            # would bust if ace valued at 11
            return False
        else :
            return True
    
    return False
# dealer_has_soft_17()

def print_hand( is_dealer, this_hand ) :
    hand_string = ""
    
    if is_dealer :
        hand_string += "Dealer's hand: "
    else :
        hand_string += "Your hand: "
        
    for this_card in this_hand :
        hand_string += deck[ this_card ][ 1 ]
        hand_string += " "
    
    hand_string = hand_string + "(value: " + str( calc_total( this_hand ) ) + ")"
    
    print( hand_string )
# print_hand()

player_hand.append( deal_card() ) # 1st player card

# dealer_hand = [ 0, 13, 26, 39 ] # debug
# deck[ 0 ][ 0 ] = 1 # debug
# deck[ 13 ][ 0 ] = 1 # debug
# deck[ 26 ][ 0 ] = 1 # debug
# deck[ 29 ][ 0 ] = 1 # debug
dealer_hand.append( deal_card() ) # 1st dealer card

player_hand.append( deal_card() ) # 2nd player card
print_hand( False, player_hand )

dealer_hand.append( deal_card() ) # 2nd dealer card (hidden card)

print( "Dealer's shown card: " + deck[ dealer_hand[ 0 ] ][ 1 ] )
print_hand( True, dealer_hand ) # debug

# ? TODO - add option for player split ?
player_hits = True
while player_hits :
    player_input = raw_input( "Would you like to hit (\"h\"), or stand (\"any other key\")? " )
    if player_input == "h" :
        player_hand.append( deal_card() )
        if calc_total( player_hand ) > max_value :
            player_hits = False
            print_hand( False, player_hand )
            print( "Bummer! You busted! Better luck next time." )
        else :
            print_hand( False, player_hand )
    else :
        player_hits = False

player_total = calc_total( player_hand )
if player_total <= max_value :
    while ( calc_total( dealer_hand ) < dealer_stands_value ) | ( ( calc_total( dealer_hand ) == dealer_stands_value ) & dealer_has_soft_17( dealer_hand ) ) :
        dealer_hand.append( deal_card() )
    
    print_hand( True, dealer_hand )
    
    dealer_total = calc_total( dealer_hand )
    if dealer_total > max_value :
        print( "The dealer busted! You win! Lucky you!" )
    else :
        if player_total < dealer_total :
            print( "Bummer! You lose. Better luck next time." )
        elif player_total == dealer_total :
            # TODO - make a Blackjack (Ace plus a ten or face card) beat a 21 of more than two cards
            print( "You tied!" )
        else :
            print( "You win! Lucky you!" )
