__author__ = "JoKamm"

print "-"*10 + "BlackJack" + "-"*10

import random

'''
Done: a deck of cards in an indexed set
Each card needs a unique id, a suit, a value. 

Needed: 
discard
resolve: deck error when it runs out
optional 1 value for A
'''

#create set of numbered cards
def deckBuilder():
    print "shuffle shuffle shuffle"
    numberCards = range(2,11)
    faceCards = ["J","Q","K"]
    
    #assign values to cards
    for i in range(len(numberCards)):
        numberCards[i] = [numberCards[i],numberCards[i]]
    
    for i in range(len(faceCards)):
        faceCards[i] = [faceCards[i],10]
    
    oneSuit = numberCards + faceCards
    oneSuit.insert(0, ["A", 11])
    
    #Create a set of each suit
    
    suits = ["C", "H", "D", "S"]
    
    deck = []
    for suit in suits:
        for card in range(len(oneSuit)):
            output = [suit] + oneSuit[card]
            deck.append(output)
    
    return deck

def dealOne(deck):
    if len(deck) == 0:
        deck = deckBuilder()
    cardNumber = random.randint(1, len(deck))
    cardOut = deck.pop(cardNumber)
    # debug: print( "len(deck) = " + str(len(deck)) )
    # Debug: print( "cardOut = " + str(cardOut))
    return cardOut

def evalHand(hand):
    handValue = 0
    for card in range( len(hand) ):
        handValue += hand[card][2]
    if handValue > 21:
        aces = 0
        for card in range(0, len(hand)):
            aces += hand[card].count("A")
        while aces > 0 and handValue > 21:
            handValue -= 10
            aces -= 1
    return handValue

def dealHand(deck, player, cards = 1):
    #deal ncards
    for i in xrange(cards):
        output = dealOne(deck)
        player.append(output)
    return player
    #deal 2 to dealer
    
def printCards(hand):
    for card in range( len( hand ) ):
        print( " " + str(hand[card][1]) + " of " + str(hand[card][0]) ),

def printHand(player):
    print( "You have: " ),
    printCards(player)
    print( "Total: " + str(evalHand(player)) )

def play(dealDeck, player):
    output = " "
    while not ((output == "H") or (output == "S")):
        output = raw_input("(H)it or (S)tay? ")
    if output == "H":
        dealHand(dealDeck, player)
        printHand(player)
    elif output == "S":
        return "S"
    return

#compare function
def handCompare(player, dealer):
    player1 = evalHand(player)
    dealer1 = evalHand(dealer)
    if dealer1 > 21 and player1 < 22:
        return "You win!"
    elif player1 > 21 and dealer1 < 22:
        return "Dealer wins!"
    elif player1 > 21 and dealer1 >21:
        return "everyone loses :("
    elif player1 > dealer1:
        return "You win!"
    elif player1 < dealer1:
        return "Dealer wins!"
    else:
        return "Tie"
   
#evaluate end value of hand
def evalEnd(hand):
    if evalHand(hand) == 21:
        output = " gets Blackjack!"
    elif evalHand(hand) > 21:
        output = " busts!"
    else:
        output = " stays at " + str(evalHand(hand)) 
    return output

def printDealer(hand):
        print( "Dealer shows: " ), 
        printCards(hand)
        print( "Total: " + str(evalHand(hand)) )
    
def startGame():
    
    #intialize hands
    player = []
    dealerShow = []
    dealerHide = []
    
    #deal to player
    dealHand(dealDeck, player, 2)
    printHand(player)
    
    #deal to dealer
    dealHand(dealDeck, dealerShow)
    dealHand(dealDeck, dealerHide)
    
    printDealer(dealerShow)
    
    #play action.  Should be in a def
    action = " "
    while action != "S" and evalHand(player) < 21 :
        action = play(dealDeck, player)
        #DEBUG print action
    
    #Print player hand status
    print( "Player" + evalEnd(player) )
    
    #dealer gameplay
    dealer = dealerShow + dealerHide
    printDealer(dealer)
    
    while evalHand(dealer) < 17:
        print( "Dealer Hits!" ),
        dealHand(dealDeck, dealer)
        printCards(dealer)
    
    print( "Dealer" + evalEnd(dealer) )
    print handCompare(player, dealer)

#gameplay

#title
#print("Blackjack")


#make a deck to deal with
dealDeck = deckBuilder()

playagain = "Y"        
while playagain == "Y":
    startGame()
    playagain = raw_input("Enter (Y)es to play again, anything else to quit: ")
    playagain = playagain.upper()
'''    
    if playagain == "Y":
        shuffle = " "
        while not (shuffle == "Y" or shuffle == "N"):
            shuffle = raw_input("Shuffle? (Y)es or (N)o: ")
            shuffle = shuffle.upper()
        if shuffle == "Y":
            dealdeck = deckBuilder()
    
    else:
        print( "Goodbye! Thanks for playing." )
     '''   



# - reshuffle?
# - play again?

    