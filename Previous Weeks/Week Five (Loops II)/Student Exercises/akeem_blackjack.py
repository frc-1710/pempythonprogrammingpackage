print "-------------- Blackjack exercise  -------------------"
#blackjack game
import random

#function to compare cards
def compareCards(total_card_player, total_card_dealer):
    if total_card_player > 21:
        return "Player total = " + str(total_card_player) + ". Player is greater than 21. Player is busted!"
    elif total_card_dealer > 21:
        return "Dealer total = " + str(total_card_dealer) + ". Dealer is greater than 21. Player wins!"
    elif total_card_player > total_card_dealer:
        return "Player total = " + str(total_card_player) + ", Dealer total = " + str(total_card_dealer) + ". Player is greater than dealer. Player wins!"
    else:
        return "Player total = " + str(total_card_player) + ", Dealer total = " + str(total_card_dealer) + ". Dealer is greater than player. Player loses!"

#create player cards
first_card_player = random.randint(1, 11)
second_card_player = random.randrange(1, 12)
total_card_player = first_card_player + second_card_player

#create dealer cards
first_card_dealer = random.randint(1, 11)
second_card_dealer = random.randrange(1, 12)
total_card_dealer = first_card_dealer + second_card_dealer

#show one of player cards
print "First player card is ", first_card_player

#show both dealer cards
print "First dealer card is ", first_card_dealer
print "Second dealer card is ", second_card_dealer

#set variable to quit game
if (total_card_dealer > 21):
    result = compareCards(total_card_player,total_card_dealer)
    print result
    quit_game = True
else:
    quit_game = False

while quit_game == False:
    user_input = raw_input("Type s to stay, h to hit and q (or any key) to quit :")
    user_input = str(user_input).upper()

    if user_input == "S":
        #user decides to stay
        result = compareCards(total_card_player,total_card_dealer)
        print result
        quit_game = True
    elif user_input == "H":
        #user decides to hit
        total_card_player += random.randint(1, 11)
        if (total_card_player > 21):
            result = compareCards(total_card_player,total_card_dealer)
            print result
            quit_game = True
    else:
        #user decides to quit
        result = compareCards(total_card_player,total_card_dealer)
        print result
        quit_game = True

#game ended
print "Game ended"