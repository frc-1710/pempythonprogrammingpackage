import random

deckStatus = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]

def playerHandValue():
    playerValue = 0
    playerAceCount = 0
    for card in playerHand:
        playerValue += valueLookup[card]
        if card == 12:
            playerAceCount += 1
    if playerAceCount >= 1:
        while (playerValue > 21) and (playerAceCount > 0):
            playerAceCount -= 1
            playerValue -= 10
    return playerValue

def drawCard():
    # print "DEBUG: drawCard called"
    while True: 
        cardCandidate = random.randint(0,12)
        # print "DEBUG: Card candidate: " + str(cardCandidate)
        # print "DEBUG: DeckStatus: " + str(deckStatus[cardCandidate])
        if deckStatus[cardCandidate] > 0: 
            deckStatus[cardCandidate] -= 1
            return cardCandidate

valueLookup = [2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11]

nameLookup = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]

dealerHand = [drawCard(), drawCard()]

playerHand = [drawCard(), drawCard()]

playerInput = "h"

print "Blackjack!"
print "Dealer stands on hard 17 or higher."
print "You start with two cards, one hidden."
print "The dealer starts with two cards, one hidden."

while playerInput != "s":
    print "Player shows: ",
    print nameLookup[playerHand[0]] + " ",
    # print "DEBUG: len(playerHand): " + str(len(playerHand))
    if len(playerHand) > 2:
        # print "DEBUG: I should be printing now"
        for i in xrange(2,len(playerHand)):
            print nameLookup[playerHand[i]] + " ",
    print ""
    showingPlayerValue = playerHandValue() - valueLookup[playerHand[1]]
    if showingPlayerValue > 21:
        print "Player busted! Gimme yo' money!"
        exit()
    print "Dealer shows: " + nameLookup[dealerHand[0]]
    while True:
        playerInput = raw_input("Hit or Stand (h/s): ")
        playerInput = playerInput.lower()
        if ((playerInput == "h") or (playerInput == "s")):
            break
    if playerInput == "s":
        break
    playerHand.append(drawCard())

playerAceCount = 0
print "Player's hand: ",
for card in playerHand:
    print nameLookup[card] + " ",
print ""
        
print "Player's hand is worth " + str(playerHandValue()) + "."

if playerHandValue() > 21:
    print "Player busted! Gimme yo' money!"
    exit()
    
dealerValue = valueLookup[dealerHand[0]] + valueLookup[dealerHand[1]]
for card in dealerHand:
    if card == 12:
        dealerValue -= 10

while dealerValue < 17:
    dealerHand.append(drawCard())
    dealerValue += valueLookup[dealerHand[-1]]
    if dealerHand[-1] == 12:
        dealerValue -= 10

print "Dealer's hand: ",
for card in dealerHand:
    print nameLookup[card] + " ",
print ""

print "Dealer's hand is worth " + str(dealerValue) + "."

if dealerValue > 21:
    print "Dealer busts! Player Wins! Take some free money!"
elif dealerValue > playerHandValue():
    print "The house wins! Gimme yo' money!"
elif playerHandValue() > dealerValue:
    print "Player wins! Take some free money!"
else:
    print "Push! You get your money back."