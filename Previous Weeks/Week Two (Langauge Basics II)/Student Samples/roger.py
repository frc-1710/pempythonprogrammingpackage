# This isn't the full assignment. It just has the logic for how I determined 
# the correct ordinal to add to the order. After converting order to a string, 
# use "string + ordinal" to get, for example, "32nd".
# mystring = "abcdefghij"
# indexes:    0123456789
# Negative
#    indexes  1987654321
#             0

for order in xrange(1,151):
    order = str(order)
    order = " " + order
    
    if order.endswith("1"):
        if order[-2] == "1":
            ordinal = "th"
        else:
            ordinal = "st"
    elif order.endswith("2"):
        if order[-2] == "1":
            ordinal = "th"
        else:
            ordinal = "nd"
    elif order.endswith("3"):
        if order[-2] == "1":
            ordinal = "th"
        else:
            ordinal = "rd"
    else:
        ordinal = "th"
        
    print order + ordinal
    
# Alternative solution:

# Before conversion to string

for order in xrange(1,151):
    if order % 10 == 1:
        if order % 100 == 11:
            ordinal = "th"
        else:
            ordinal = "st"
    elif order % 10 == 2:
        if order % 100 == 12:
            ordinal = "th"
        else:
            ordinal = "nd"
    elif order % 10 == 3:
        if order % 100 == 13:
            ordinal = "th"
        else:
            ordinal = "rd"
    else:
        ordinal = "th"
    order = str(order)
    print order + ordinal

# Convert order to string and go.