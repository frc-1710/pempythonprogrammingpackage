state_dict = {'AL' : 'Alabama', 'AK' : 'Alaska', 'AZ' : 'Arizona', 'AR' : 'Arkansas', 'CA' : 'California','CO' : 'Colorado', 'CT' : 'Connecticut','DE' : 'Delaware','FL' : 'Florida','GA' : 'Georgia','HI' : 'Hawaii','ID' : 'Idaho','IL' : 'Illinois','IN' : 'Indiana','IA' : 'Iowa','KS' : 'Kansas','KY' : 'Kentucky','LA' : 'Louisiana','ME' : 'Maine','MD' : 'Maryland','MA' : 'Massachusetts','MI' : 'Michigan','MN' : 'Minnesota','MS' : 'Mississippi','MO' : 'Missouri','MT' : 'Montana','NE' : 'Nebraska','NV' : 'Nevada','NH' : 'New Hampshire','NJ' : 'New Jersey','NM' : 'New Mexico','NY' : 'New York','NC' : 'North Carolina','ND' : 'North Dakota','OH' : 'Ohio','OK' : 'Oklahoma','OR' : 'Oregon','PA' : 'Pennsylvania','RI' : 'Rhode Island','SC' : 'South Carolina','SD' : 'South Dakota','TN' : 'Tennessee','TX' : 'Texas','UT' : 'Utah','VT' : 'Vermont','VA' : 'Virginia','WA' : 'Washington','WV' : 'West Virginia','WI' : 'Wisconsin','WY' : 'Wyoming','DC' : 'District of Columbia'}

# List of states in the order they joined the union
state_list = ['Delaware', 'Pennsylvania', 'New Jersey', 'Georgia', 'Connecticut', 'Massachusetts', 'Maryland', 'South Carolina', 'New Hampshire', 'Virginia', 'New York', 'North Carolina', 'Rhode Island', 'Vermont', 'Kentucky', 'Tennessee', 'Ohio', 'Louisiana', 'Indiana', 'Mississippi', 'Illinois', 'Alabama', 'Maine', 'Missouri', 'Arkansas', 'Michigan', 'Florida', 'Texas', 'Iowa', 'Wisconsin', 'California', 'Minnesota', 'Oregon', 'Kansas', 'West Virginia', 'Nevada', 'Nebraska', 'Colorado', 'North Dakota', 'South Dakota', 'Montana', 'Washington', 'Idaho', 'Wyoming', 'Utah', 'Oklahoma', 'New Mexico', 'Arizona', 'Alaska', 'Hawaii']

    
while True:

    # extra credit: (hint treat the string like a list)
    # if it ends in a "2" add "nd" at the end of the string
    # else if it ends in a "1" add "st" at the end of the string
    # else if it ends in a "3" add "rd" at the end of the string
    # else, add a "th" to the end of the string
    # for extra, extra credit - account for the exceptions 11th, 12th, and 13th
    
    state_short = raw_input("Please give me the state abbreviation:")
    state_long = state_dict.get(state_short)
    if not state_long:
        if state_short == "quit" or state_short == "q":
            print "fine, we're done, take care!"
            exit()
            
        print "silly, %s is not a state abbreviation. If you want to quit type 'quit' or 'q'" % state_short
        continue
    state_entry = state_list.index(state_long) + 1
    state_message = "%s is the %s%s state in the union"
    l_num = int(str(state_entry)[len(str(state_entry)) - 1:])
    
    if (9 < state_entry < 20) or (l_num is 0 or 3 < l_num < 10):
        suffix = "th"
    elif l_num is 3:
        suffix = "rd"
    elif l_num is 2:
        suffix = "nd"
    elif l_num is 1:
        suffix = "st"
    else:
        suffix = "th"
        
    print(state_message % (state_long, state_entry, suffix))
    