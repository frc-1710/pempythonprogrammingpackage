import random

#defined variables
__heads__ = 0
__tails__ = 1
heads = "H"
tails = "T"

def coinQuery():
    nCoins = "x"
    while not nCoins.isdigit():
        nCoins = raw_input("How many coins do you want to flip? (e.g. 1,2,3) ")
    nCoins = int(nCoins)
    # print(nCoins)
    myGuessList = coinGuess(nCoins)
    return myGuessList

#Alternatively, the prompt could be for n guesses, and that could determine the number of coins tossed, rather than asking for the number and the guesses separately
    
def coinGuess(nCoins):
    guesses = []
    print "Guess (H)eads or (T)ails"
    guessList = raw_input("Give "+ str(nCoins) +" guesses. (e.g. HTHH...)")
    for char in guessList:
        if not len(guessList) == nCoins:
            print ("please make "+ str(nCoins) +" guesses")
            return coinGuess(nCoins)
        elif char == "H" or char == "h":
            guesses.append(heads)
        elif char == "T" or char == "t":
            guesses.append(tails)  
        else:
            print ("please use only H or T, with no spaces.")
            return coinGuess(nCoins)
    return guesses

def coinFlipOne():
    ''' This function takes no variable and returns a single 'H' or 'T'.'''  
    flip = random.randint(0,1)
    if flip == 0:
        flip = heads
    else:
        flip = tails  
    return flip

def coinCompare():
    '''Compares a list of user-generated coin flip guesses with a list of random "flips". 
    Results are collected in guessRecord list.
    First item is the number of correct guesses.
    '''
    userGuessList = coinQuery()
    flipList = []
    #test for how many items are in userGuessList
    for i in range(len(userGuessList)):
        flipList.append(coinFlipOne())
    #print flipList
    #initialize guessRecord with 1st value as sum of correct guesses
    guessRecord = [0]
    for i in range(len(userGuessList)):
        if userGuessList[i] == flipList[i]:
            guessRecord[0] += 1
            guessRecord.append(True)
        else:
            guessRecord.append(False)
    guessRate = float(guessRecord[0]) / len(flipList) * 100
    print("You guessed:   " + str(userGuessList))
    print("Coins flipped: " + str(flipList))
    print("You guessed correctly " + str(guessRate) + "% of the time.")
    return
    
print("Welcome to the Super Awesome Coin Tosser!")
while True:
    toss = raw_input("Do you want to try guessing a coin toss? (Y/N)")
    if toss == "Y":
        coinCompare()
    else:
        print("Goodbye!")
        break
    

