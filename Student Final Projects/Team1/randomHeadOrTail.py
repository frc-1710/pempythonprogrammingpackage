#randomHeadOrTail
# This module is responsible for generating heads or tails randomly
__author__ = 'akeemakinola'

# import modules
import random

# helper function to generate random integer
def generateRandomInt():
    """ generate a random integer between 0 and 1 """
    randomInt = random.randint(0,1)
    return randomInt

# helper function to return heads or tails based on input
#  if input is invalid, function will return an "error" string
#  otherwise return value will be heads or tails
def generateHeadsOrTails(value):
    """ validate input """
    if (validateIntEntry(value)):
        """ return heads or tails """
        if (int(value) == 1):
            return "heads"
        else:
            return "tails"
    else:
        """ return an error string """
        return "error"
        
# helper function to validate entry
#  valid inputs are 0 or 1; any other input will throw an error and
#  return False as the output; otherwise it will return True for a success
def validateIntEntry(value):
    """ check that input is a valid integer """
    try:
        val = int(value)
        assert(val == 0 or val == 1)
    except (ValueError, AssertionError):
        print("ERROR - validateIntEntry - received an valid entry!")
        return False
    else:
        return True

# main function to generate heads or tails
#  will return "-1" if an error occurs, else will return 1 for success
def generateRandomHeadOrTail():
    randomEntry = generateRandomInt()
    generatedValue = generateHeadsOrTails(randomEntry)
    if(generatedValue == "error"):
        print "An error occured.. random entry could not be generated"
        return "-1"
    else:
        return generatedValue

#test for valid input
# uncomment these lines to test this module
# for count in xrange(1,6):
#     print generateRandomHeadOrTail()

#test for invalid input
# uncomment these lines to test this module
# randomEntry = "3.0"
# print "Entry = ", str(randomEntry)
# print "Value of randomly generated entry =", str(generateHeadsOrTails(randomEntry)), "\n"
# randomEntry = "invalidInput"
# print "Entry = ", str(randomEntry)
# print "Value of randomly generated entry =", str(generateHeadsOrTails(randomEntry)), "\n"