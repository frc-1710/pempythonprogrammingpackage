#Note to Akeem and Marie.  My little section is not working and I've been playing with it for a couple of hours.
#Going to stop, and won't be able to get on until the weekend.  Maybe you can use some of it, but I don't
#know why the if statement is invalid.  Tried all kinds of indents and various placements and just need more time.


import random
myflip = random.randint(0,1)
print (myflip)

playerinput = raw_input("Do you want to flip? Y or N")
while playerinput != "Y":
    #break               #[AKEEM] This break isn't in the right place. If the user inputs N, the code will never get past here. You can take this out.
    if myflip == 1:      #[AKEEM] The if statment is invalid because you are doing an assignment (=) instead of a comparision (==). The right thing should be "if myflip == 1:"
        print "heads"
    else:
        print "tails"
break                    #[AKEEM] This break needs to be indented to terminate the while loop. Indent it to the same vertical position as the if else.