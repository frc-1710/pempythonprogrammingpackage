#flipCoins
# This module is responsible for flipping coins
__author__ = 'akeemakinola'

# import modules
import randomHeadOrTail

# function to get computer flip
#  will return either heads or tails
def getComputerFlip():
    """ flip coin and return flip value """
    return randomHeadOrTail.generateRandomHeadOrTail()

# function to get user guess
#  will return one of the following values
#    - heads/tails : based on userEntry, function will return heads for an 
#       input of 1 and tails for an input of 2
#    - quit : will return this if the user inputs a q or Q
#    - error : will return this if any other input is supplied
def getUserGuess(userEntry):
    """ quit if input is q """
    if (str(userEntry).lower() == "q"):
        return "quit"
        
    """ check that input is a valid integer """
    if (randomHeadOrTail.validateIntEntry(userEntry)):
        """ return heads or tails based on input """
        return randomHeadOrTail.generateHeadsOrTails(userEntry)
    else:
        """ invalid entry """
        return "error"

        
#test
# uncomment these lines to test this module
# computerGeneratedFlip = getComputerFlip()
# print "Computer flip =", computerGeneratedFlip
# userEntry = raw_input("Please enter 1 for heads; 0 for tails or q to quit :")
# playerGuess = getUserGuess(userEntry)
# if (playerGuess != "error"):
#     print "User guess =", playerGuess