#comparator
# This module is responsible for comparing the user guess with the computer flip
__author__ = 'akeemakinola'

# import modules
import flipCoins

# store globals
playerGuess = "" 
computerGeneratedFlip = ""

# function to compare the user guess with the computer flip
#  will return the result of the comparison; True if they match, False otherwise
def compareGuessWithFlip(userEntry):
    """ declare globals """
    global playerGuess, computerGeneratedFlip
    
    """ get flip and guess values """
    computerGeneratedFlip = flipCoins.getComputerFlip()
    playerGuess = flipCoins.getUserGuess(userEntry)
    
    """ check return of playerGuess before comparison """
    """ handle error and quit cases """
    if (playerGuess == "error"):
        print "Error occured.. cannot continue!"
    elif (playerGuess == "quit"):
        print "User quits... Result cannot be compared!"
    else:
        """ compare guess to flip """
        return(computerGeneratedFlip==playerGuess)
        
    return playerGuess

#test
# uncomment these lines to test this module
# print "User guessed heads - Result is", compareGuessWithFlip(1)
# print "User guessed tails - Result is",compareGuessWithFlip(0)
# print compareGuessWithFlip("q")
# print "User supplies invalid input - Result is",compareGuessWithFlip("invalid")