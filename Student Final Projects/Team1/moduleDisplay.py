#moduleDisplay
# This module is responsible for displaying the outcome of the flips until user stops
__author__ = 'akeemakinola'

# import modules
import comparator

# function to display the outcome of the comparisions
def displayOutcome():
    """ initialize loop variable """
    loop = True
    while (loop):
        
        # get user entry
        userEntry = raw_input("Please enter 1 for heads; 0 for tails or q to quit :")
        
        # compare guess with flip
        result = comparator.compareGuessWithFlip(userEntry)
        
        # quit on error on user input of q
        if((result == "error") or (result == 'quit')):
            loop = False
            break
        
        # print out result of comparison
        print "Computer flip =", comparator.computerGeneratedFlip, "; Player guess =", comparator.playerGuess
        if(result):
            print "Woohoo!! Player guessed correctly!"
        else:
            print "Oh no!! Player's guess was incorrect."
        print""

#test
# uncomment these lines to test this module
# displayOutcome()