#!/usr/bin/python

def getCount():
    count = int(0)
    while (count <= 0) and (count == int(count)):
        count = raw_input("How many coins do you want to flip? ")
        try:
            count = int(count)
        except(ValueError):
            print "That's a string! How am I supposed to turn a string into an integer?"
            count = getCount()
        if ((count <= 0) and (count == int(count))):
            print "The count must be a positive integer."
            print "I can't un-flip a coin."
    return count

count = getCount()

print "The count is: " + str(count)