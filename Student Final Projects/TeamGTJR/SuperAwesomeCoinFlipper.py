#!/usr/bin/python

__author__ = "Roger Cook"

'''
SuperAwesomeCoinFlipper is the master control file that the user will run.

It will do the following tasks:

1. Collect a guess from the user.
2. Call the coinFlip(n) function and receive a list of guesses.
3. Call the coinCompare(coin,guess) function and receive a Boolean.
4. Feed these into the coinDisplay() function to print them.
'''

#TH: Changed the name of flipOne.py to coinFlipOne.py. It returns H or T for variable flipped.

import coinFlip
import coinCompare
import coinDisplay

print "SuperAwesomeCoinFlipper"

'''
Collect the user's guess
'''
def getGuess():
    guess = "X"
    while (guess != "H") and (guess != "T"):
        guess = raw_input("Do you guess (H)eads or (T)ails? ")
        guess = guess.upper()
        if ((guess != "H") and (guess != "T")):
            print "You must guess either (H)eads or (T)ails."
            print "Please enter either H or T."
            print "This isn't the Twilight Zone. Coins don't land on their sides."
    return guess

def getAgain():
    again = "X"
    while (again != "Y") and (again != "N"):
        again = raw_input("Do you want to try again? (Y/N): ")
        again = again.upper()
        if ((again != "Y") and (again != "N")):
            print "Please answer (Y)es or (N)o."
            print "Please enter either Y or N."
    return again

'''
Collect the user's number of coins to flip
'''

def getCount():
    count = int(0)
    while (count <= 0) and (count == int(count)):
        count = raw_input("How many coins do you want to flip? ")
        try:
            count = int(count)
        except(ValueError):
            print "That's a string! How am I supposed to turn a string into an integer?"
            count = getCount()
        if ((count <= 0) and (count == int(count))):
            print "The count must be a positive integer."
            print "I can't un-flip a coin."
    return count

doItAgain = "Y"

while doItAgain == "Y":

    count = getCount()

    guessList = []      #Sets empty list.

    for i in xrange(count):
        j = i + 1
        print "Guess " + str(j)
        guessList.append(getGuess())
    
    '''
    https://docs.python.org/2.7/tutorial/controlflow.html?highlight=break#for-statements
    '''
    
    '''
    Flip the coins
    '''
    
    coinList = coinFlip.coinFlip(count)
    
    # print "DEBUG: Coinlist is " + str(coinList)
    
    # print "DEBUG: coinList is a " + str(type(coinList))
    
    '''
    Start comparing them
    '''
    
    resultList = []
    
    for i in xrange(count):
        result = coinCompare.coinCompare(coinList[i],guessList[i])
        resultList.append(result)
    
    print "The results are in!"
    
    numCorrect = 0
    j = 1
    
    for i in xrange(count):
        print "\t" + str(j) + ":",
        coinDisplay.coinDisplay(coinList[i],resultList[i])
        if (resultList[i]):
            numCorrect += 1
        j += 1
    
    headsCount = 0
    tailsCount = 0
    
    for coin in coinList:
        if coin == "H":
            headsCount += 1
        else:
            tailsCount += 1
    
    
    print ""
    print "You got " + str(numCorrect) + " correct out of " + str(count) + "."
    
    percentageCorrect = '{:.2%}'.format(float(numCorrect) / float(count))
    percentageHeads = '{:.2%}'.format(float(headsCount) / float(count))
    percentageTails = '{:.2%}'.format(float(tailsCount) / float(count))
    
    
    
    print "You guessed " + percentageCorrect + " of the coins correctly."
    print percentageHeads + " of the coins were heads."
    print percentageTails + " of the coins were tails."
    
    doItAgain = getAgain()