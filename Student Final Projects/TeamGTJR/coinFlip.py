#!/usr/bin/python

_author_ = "Giano Rodriguez"

'''
This file must import "flipOne"         TH: Changed the name of flipOne.py to coinFlipOne.py, so I can put the
                                        Just following the design file. It will return an H or a T for variable
                                        flipped in coinFlipOne.py

This file cointains a single function, coinFlip(n) that will return a list 
of strings either H or T, with a length equal to n. The individual H or T will 
come from a call to the flipOne.flipOne() function.
'''

import coinFlipOne

def coinFlip(n):
    coinList = [] #empty container for H or T flips
#     flipOne = "H" or "T"
    for i in xrange(n):
        coinList.append(coinFlipOne.flipOne())
    return coinList