#WHAT'S MY NAME?

import string   #import string library

name = raw_input("What are your first and last names? ")    #ask for first and last names as input
#print(name)    debug

FirstName, LastName = name.split()

length_name = len(name)     #find the length of the full string
#print(length_name)     debug
length_fn = len(FirstName)
length_ln = len(LastName)
FirstInit = FirstName[0]
# print FirstInit   #debug initial generation
LastInit = LastName[0]


print(name, FirstName, LastName)
print("The length of your first name is " + str(length_fn) + ".")
print("The length of your last name is " + str(length_ln) + ".")
print("Your initials are " + str(FirstInit) + str(LastInit) + ".")