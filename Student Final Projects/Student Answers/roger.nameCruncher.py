#!/usr/bin/PYTHON

# 1. Takes in user input of name
# 2. Finds and outputs:
#    a. First name
#    b. Last name
#    c. Length of each name
#    d. Initials

# import string

# from __future__ import print_function

print("Name Cruncher")
print("Please type in your first and last name with a space between them.")
inputName = raw_input("""For Example: "John Doe": """)

firstName,space,lastName = inputName.partition(" ")


print "The first name is: " + firstName
print "The last name is: " + lastName

print "The length of the first name is: " + str(len(firstName))
print "The length of the last name is: " + str(len(lastName))

print "The initials are: " + firstName[0] + lastName[0]