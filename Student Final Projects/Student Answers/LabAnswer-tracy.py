__author__ = 'halgottfried'

try:
    name = raw_input('What is your name and the year you were born \n (Format: First Name Last Name YOB) ?')
    firstName, LastName, Year = name.split()

except(ValueError):
    print "That is not enough items."
    print "Format: First Name <space> Last Name <space> Year of Birth"
    # Exits our program if the error is caught.
    raise SystemExit #TH breaks out of this section. Use instead of break?

else:
    def hasnumbers(name):   #TH defines a function, hasnumbers IDE made him ditch camelcase
        return any(char.isdigit() for char in name) #TH this returns which character is a digit in the name, if a digit exists. name is the range for the for. Checks to see if each digit in sequence is or is not a digit. This does not actually DO the comparison until it's called.
    #TH This is the other side of the try.

if hasnumbers(firstName) or hasnumbers(LastName):       #TH Now, we're calling the function we defined. If you do an "if" on an already-defined boolean, it expects a true, and won't fail unless it sees a false. (Or do I have that backward?)
    print ('I am sorry but you seem to have spelled your name wrong')
    raise SystemExit #TH breaks out of this section. Use instead of break?
else:
    firstLen = len(firstName)
    lastLen = len(LastName)

    print ('The entered first name ' + (firstName) + ' is ' + str(firstLen) + ' letters long.')
    print ('The entered last name ' + (LastName) + ' is ' + str(lastLen) + ' letters long.')

if int(Year)>=int(2014):
    print "You haven't been born, yet..."
    raise SystemExit

try:
      print 'Your age is: ' + str(int(2014) - int(Year))
      #Not very elegant should have used datetime but it works.
      #TH Tested this until he found the exception.

except(ValueError):
     print 'Your age is not a correct value'
     raise SystemExit
     
